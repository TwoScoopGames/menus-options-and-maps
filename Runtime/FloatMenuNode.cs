using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class FloatMenuNode : MenuNode {
    public FloatReference option;
    public float minimum;
    public float maximum;

    public override bool MoveLeft() {
      if (option.Value > minimum) {
        option.Value = (float)Mathf.Round((option.Value - 0.05f) * 100f) / 100f;
        moveLeft.Invoke();
        return true;
      }
      return false;
    }

    public override bool MoveRight() {
      if (option.Value < maximum) {
        option.Value = (float)Mathf.Round((option.Value + 0.05f) * 100f) / 100f;
        moveRight.Invoke();
        return true;
      }
      return false;
    }
  }
}
