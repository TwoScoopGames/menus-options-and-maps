using UnityEngine;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class MenuCursor : MenuCursorBase {
    public float speed = 0.5f;

    void Update() {
      transform.position = Vector3.Lerp(transform.position, currentNode.transform.position, speed);
    }
  }
}
