using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class BooleanMenuNode : MenuNode {
    public BoolReference option;

    public override void Activate() {
      option.Value = !option.Value;
      base.Activate();
    }

    public override bool MoveLeft() {
      Activate();
      return false;
    }

    public override bool MoveRight() {
      Activate();
      return false;
    }
  }
}
