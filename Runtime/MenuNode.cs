using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class MenuNode : MonoBehaviour {
    [FormerlySerializedAs("onUp")]
    public MenuNode up;
    [FormerlySerializedAs("onDown")]
    public MenuNode down;
    [FormerlySerializedAs("onLeft")]
    public MenuNode left;
    [FormerlySerializedAs("onRight")]
    public MenuNode right;

    [FormerlySerializedAs("onHighlight")]
    public UnityEvent onSelect;
    public UnityEvent onDeselect;
    public UnityEvent onActivate;
    public UnityEvent moveUp;
    public UnityEvent moveDown;
    public UnityEvent moveLeft;
    public UnityEvent moveRight;

    public virtual void Select() {
      onSelect.Invoke();
    }

    public virtual void Deselect() {
      onDeselect.Invoke();
    }

    public virtual void Activate() {
      onActivate.Invoke();
    }

    public virtual bool MoveUp() {
      if (!up) {
        return false;
      }
      moveUp.Invoke();
      return true;
    }

    public virtual bool MoveDown() {
      if (!down) {
        return false;
      }
      moveDown.Invoke();
      return true;
    }

    public virtual bool MoveLeft() {
      if (!left) {
        return false;
      }
      moveLeft.Invoke();
      return true;
    }

    public virtual bool MoveRight() {
      if (!right) {
        return false;
      }
      moveRight.Invoke();
      return true;
    }
  }
}
