using TwoScoopGames.ScriptableObjects.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class IntMenuNode : MenuNode {
    public IntReference option;
    public int minimum;
    public int maximum;

    public override bool MoveLeft() {
      if (option.Value > minimum) {
        option.Value--;
        moveLeft.Invoke();
        return true;
      }
      return false;
    }

    public override bool MoveRight() {
      if (option.Value < maximum) {
        option.Value++;
        moveRight.Invoke();
        return true;
      }
      return false;
    }
  }
}
