﻿using UnityEngine;
using UnityEngine.Events;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public abstract class LockableMenuNode : MenuNode {
    public GameObject beatenLevel;
    public GameObject nextLevel;
    public GameObject lockedLevel;

    protected virtual void Start() {
      beatenLevel.SetActive(false);
      nextLevel.SetActive(false);
      lockedLevel.SetActive(false);

      if (!Locked()) {
        beatenLevel.SetActive(true);
      } else if (!IsLocked(up)) {
        nextLevel.SetActive(true);
      } else if (!IsLocked(down)) {
        nextLevel.SetActive(true);
      } else if (!IsLocked(left)) {
        nextLevel.SetActive(true);
      } else if (!IsLocked(right)) {
        nextLevel.SetActive(true);
      } else {
        lockedLevel.SetActive(true);
      }
    }

    private static bool IsLocked(MenuNode node) {
      if (node == null) {
        return true;
      }
      var lmn = node as LockableMenuNode;
      if (lmn == null) {
        return false;
      }
      return lmn.Locked();
    }

    public abstract bool Locked();

    public override bool MoveUp() {
      return TryMove(up, moveUp);
    }

    public override bool MoveDown() {
      return TryMove(down, moveDown);
    }

    public override bool MoveLeft() {
      return TryMove(left, moveLeft);
    }

    public override bool MoveRight() {
      return TryMove(right, moveRight);
    }

    private bool TryMove(MenuNode node, UnityEvent evt) {
      if (!node) {
        return false;
      }

      var lockableNode = node as LockableMenuNode;
      if (Locked() && lockableNode && lockableNode.Locked()) {
        return false;
      }

      evt.Invoke();
      return true;
    }
  }
}
