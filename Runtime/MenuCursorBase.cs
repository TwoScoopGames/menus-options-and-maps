using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace TwoScoopGames.MenusOptionsAndMaps {
  public class MenuCursorBase : MonoBehaviour {
    public MenuNode currentNode;

    public UnityEvent onActivate;
    public UnityEvent onUp;
    public UnityEvent onDown;
    public UnityEvent onLeft;
    public UnityEvent onRight;
    public UnityEvent onInvalidMove;

    void Start() {
      if (currentNode) {
        currentNode.Select();
      }
    }

    public void MoveUp(InputAction.CallbackContext context) {
      if (!context.started) {
        return;
      }
      if (currentNode.MoveUp()) {
        onUp.Invoke();
        SelectNode(currentNode.up);
      } else {
        onInvalidMove.Invoke();
      }
    }

    public void MoveDown(InputAction.CallbackContext context) {
      if (!context.started) {
        return;
      }
      if (currentNode.MoveDown()) {
        onDown.Invoke();
        SelectNode(currentNode.down);
      } else {
        onInvalidMove.Invoke();
      }
    }

    public void MoveLeft(InputAction.CallbackContext context) {
      if (!context.started) {
        return;
      }
      if (currentNode.MoveLeft()) {
        onLeft.Invoke();
        SelectNode(currentNode.left);
      } else {
        onInvalidMove.Invoke();
      }
    }

    public void MoveRight(InputAction.CallbackContext context) {
      if (!context.started) {
        return;
      }
      if (currentNode.MoveRight()) {
        onRight.Invoke();
        SelectNode(currentNode.right);
      } else {
        onInvalidMove.Invoke();
      }
    }

    public void SelectNode(MenuNode node) {
      if (node == null) {
        return;
      }
      if (currentNode) {
        currentNode.Deselect();
      }
      currentNode = node;
      currentNode.Select();
    }

    public void ActivateNode(InputAction.CallbackContext context) {
      if (!context.started) {
        return;
      }
      onActivate.Invoke();
      currentNode.Activate();
    }
  }
}
